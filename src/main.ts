import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { tokenMicroserviceOptions } from './config/grpc.options';
import { MicroserviceOptions } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    tokenMicroserviceOptions
  );
  app.listen(() => console.log('Microservice is listening'));
}
bootstrap();
