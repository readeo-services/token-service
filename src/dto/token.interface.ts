export interface ITokenListMsg {
  userId: string;
  languageCode: string;
  page: number;
}
