import { Injectable } from "@nestjs/common";
import { InjectModel } from '@nestjs/mongoose';
import { Model } from "mongoose";
import { Token } from "./entity/token.interface";
import { ITokenListMsg } from "../dto/token.interface";

@Injectable()
export class TokensService {
  constructor(
    @InjectModel("Token") private readonly tokenModel: Model<Token>
  ) { }

  async findByLanguage(params: ITokenListMsg, userId: string): Promise<Token[]> {
    return this.tokenModel.find({lang_code: params.languageCode, user_id: userId}).limit(100).skip(params.page*100)
  }
}
