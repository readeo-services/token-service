import { Controller } from '@nestjs/common';
import { TokensService } from './tokens.service';
import { ITokenListMsg } from '../dto/token.interface';
import { Token } from './entity/token.interface';
import { GrpcMethod } from '@nestjs/microservices';
import { Metadata } from 'grpc';

@Controller("tokens")
export class TokensController {
  constructor(private readonly tokenService: TokensService) {}

  @GrpcMethod()
  async getTokenList(params: ITokenListMsg, metadata: Metadata): Promise<Token[]> {
    const ctx = metadata.getMap()
    console.log(ctx.userId)
    return this.tokenService.findByLanguage(params, ctx.userId.toString())
  }
}
