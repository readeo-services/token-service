import * as mongoose from "mongoose";
import { Token } from "./token.interface";

export const TokenSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  languageCode: {
    type: String,
    required: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'UserSchema'
  },
  level: {
    type: Number,
    required: true,
    default: 0
  },
  definition: {
    type: String
  },
  updatedAt: {
    type: Number,
    required: true
  },
  serverUpdatedAt: {
    type: Number,
    required: true
  }
});

TokenSchema.index({ name: 1, languageCode: 1, userId: 1 }, { unique: true })

TokenSchema.set("toJSON", {
  transform: (_doc: Token, ret: Token) => {
    return (({ userId, name, languageCode, level, definition, updatedAt }) => ({
      userId,
      name,
      languageCode,
      level,
      definition,
      updatedAt,
    }))(ret);
  },
});
