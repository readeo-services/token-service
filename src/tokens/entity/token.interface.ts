import { Document } from "mongoose";

export interface Token extends Document {
  readonly name: string;
  readonly definition: string;
  readonly level: number;
  readonly languageCode: string;
  readonly updatedAt: number;
  readonly userId: string
}
