import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TokensController } from './tokens.controller';
import { TokensService } from './tokens.service';
import { TokenSchema } from './entity/token.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "Token", schema: TokenSchema }]),],
  controllers: [TokensController],
  providers: [TokensService],
})
export class TokensModule { }
