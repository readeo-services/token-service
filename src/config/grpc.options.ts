import { ClientOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';

export const tokenMicroserviceOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    package: 'token',
    url: '0.0.0.0:5000',
    protoPath: join(__dirname, '../../src/proto/token.proto'),
  },
};

