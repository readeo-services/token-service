import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TokensModule } from './tokens/tokens.module';

@Module({
  imports: [MongooseModule.forRoot(process.env.MONGOHQ_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  }),TokensModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
